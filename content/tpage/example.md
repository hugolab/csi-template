+++
title = "Example Page"

date = 2018-09-09T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
#toc = true  # Show table of contents? true/false
type = "tpage"  # Do not modify.

widget = "projects"
external_link = "/people/euiseong/"
# Add menu entry to sidebar.
linktitle = "Euiseong"
[menu.tpage]
  parent = "Professor"
  weight = 1
+++

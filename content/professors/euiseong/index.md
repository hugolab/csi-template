+++
# Project title.
title = "Euiseong Seo"


# Date this page was created.
date = 2016-04-27T00:00:00

# Project summary to display on homepage.
summary = "Computer System Lab."

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["Prof"]
weight = 2
# Optional external URL for project (replaces project detail page).
external_link = "/people/euiseong/"

#[permalinks]
#    post = "http://www.naver.com"

# Featured image
# To use, add an image named `featured.jpg/png` to your project's folder. [image]
  # Caption (optional)
  caption = "Photo by Toa Heftiba on Unsplash"

  # Focal point (optional)
  # Options: Smart, Center, TopLeft, Top, TopRight, Left, Right, BottomLeft, Bottom, BottomRight
  focal_point = "Smart"
+++

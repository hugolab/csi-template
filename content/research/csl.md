+++
title = "Computer System Research"

date = 2018-09-09T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
linktitle = "Computer System"
[menu.research]
  parent = "Research"
  weight = 2


+++

## **Research interests**
We are addressing emerging challenges in computer systems with an emphasis on performance, cost, reliability, and energy consumption. Current research interests include (but are not limited to):

* System software for flash memory
* Embedded systems
* Virtualization
* Energy-efficient computing
* Storage systems
* Distributed systems
* Cluster/Grid/Cloud computing
* Operating systems
* Computer architecture

## **Activities**
We are hosting the following exciting projects:
* The OpenSSD Project
* AndroBench - A storage benchmark for Android devices

## **Research highlights**
* OwFS: Large-scale Distributed File System (Joint work with NHN Corp. from 2006 to 2008)
Featured in NHN Story (June 23, 2008)
Related article in the KIISE Newsletter (in Korean, May 2009)

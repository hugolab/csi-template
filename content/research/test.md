+++
title = "Computer System Research"

date = 2018-09-09T00:00:00
# lastmod = 2018-09-09T00:00:00

draft = false  # Is this a draft? true/false
toc = true  # Show table of contents? true/false
type = "docs"  # Do not modify.

# Add menu entry to sidebar.
[menu.research]
  parent = "Computer System"
  name = "ttttt"
  weight = 2


+++

This feature can be used for publishing content such as:

* **Project or software documentation**
* **Online courses**
* **Tutorials**

The parent folder may be renamed, for example, to `docs` for project documentation or `course` for creating an online course.

To disable this feature, either delete the parent folder, or set `draft = true` in the front matter of all its pages. 

After renaming or deleting the parent folder, you may wish to update any `[[menu.main]]` menu links to it in the `config.toml`. 


## **Research interests**
We are addressing emerging challenges in computer systems with an emphasis on performance, cost, reliability, and energy consumption. Current research interests include (but are not limited to):

* System software for flash memory
* Embedded systems
* Virtualization
* Energy-efficient computing
* Storage systems
* Distributed systems
* Cluster/Grid/Cloud computing
* Operating systems
* Computer architecture

## **Activities**
We are hosting the following exciting projects:
* The OpenSSD Project
* AndroBench - A storage benchmark for Android devices

## **Research highlights**
* OwFS: Large-scale Distributed File System (Joint work with NHN Corp. from 2006 to 2008)
Featured in NHN Story (June 23, 2008)
Related article in the KIISE Newsletter (in Korean, May 2009)

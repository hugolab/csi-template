+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Associate Professor"
  company = """
  College of Information and Communication Engineering
  Sungkyunkwan University
  """
  company_url = ""
#  location = "California"
  date_start = "2013-01-01"
  date_end = ""
#  description = """
#  Responsibilities include:
#  
#  * Analysing
#  * Modelling
#  * Deploying
#  """

[[experience]]
  title = "Assistant Professor"
  company = """
  College of Information and Communication Engineering
  Sungkyunkwan University
  """
  company_url = ""
  date_start = "2012-01-01"
  date_end = "2013-01-01"

[[experience]]
  title = "Assistant Professor"
  company = """
  School of ECE, Ulsan National Institute of Science and Technology (UNIST)
  """
  company_url = ""
  date_start = "2009-01-01"
  date_end = "2012-12-01"

[[experience]]
  title = "Research Associate"
  company = "CSE Dept., Penn State University"
  company_url = ""
#  location = "California"
  date_start = "2007-01-01"
  date_end = "2009-12-31"
#  description = """Taught electronic engineering and researched semiconductor physics."""

+++

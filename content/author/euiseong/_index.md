+++
# Display name
name = "Euiseong Seo"

# Is this the primary user of the site?
superuser = false

# Role/position
role = "Associate Professor on College of Information and Communication Engineering"

# Organizations/Affiliations
#   Separate multiple entries with a comma, using the form: `[ {name="Org1", url=""}, {name="Org2", url=""} ]`.
organizations = [ { name = "Sungkyunkwan University", url = "" } ]

# Short bio (displayed in user profile at end of posts)
bio = ""

# Enter email to display Gravatar (if Gravatar enabled in Config)
email = "euiseong@gmail.com"

# List (academic) interests or hobbies
interests = [
  "-",
  "-",
  "-"
]

# List qualifications (such as academic degrees)
[[education.courses]]
  course = "PhD in Computer Science"
  institution = "Korea Advanced Institute of Science and Technology(KAIST)"
  year = 2007

[[education.courses]]
  course = "MS in Computer Science"
  institution = "Korea Advanced Institute of Science and Technology(KAIST)"
  year = 2002

[[education.courses]]
  course = "BS in Computer Science"
  institution = "Korea Advanced Institute of Science and Technology(KAIST)"
  year = 2000

awards = [
  "-",
  "-",
  "-"
]


# Social/Academic Networking
#
# Icon pack "fab" includes the following social network icons:
#
#   twitter, weibo, linkedin, github, facebook, pinterest, google-plus,
#   youtube, instagram, soundcloud
#
#   For email icon, use "fas" icon pack, "envelope" icon, and
#   "mailto:your@email.com" as the link.
#
#   Full list: https://fontawesome.com/icons
#
# Icon pack "ai" includes the following academic icons:
#
#   cv, google-scholar, arxiv, orcid, researchgate, mendeley
#
#   Full list: https://jpswalsh.github.io/academicons/

[[social]]
  icon = "envelope"
  icon_pack = "fas"
  link = "#contact"  # For a direct email link, use "mailto:test@example.org".

[[social]]
  icon = "twitter"
  icon_pack = "fab"
  link = "https://twitter.com/GeorgeCushen"

[[social]]
  icon = "google-scholar"
  icon_pack = "ai"
  link = "https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ"

[[social]]
  icon = "github"
  icon_pack = "fab"
  link = "https://github.com/gcushen"

# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# [[social]]
#   icon = "cv"
#   icon_pack = "ai"
#   link = "files/cv.pdf"

+++

-
